var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "8385",
        "ok": "8385",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "80",
        "ok": "80",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7373",
        "ok": "7373",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "percentiles1": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles2": {
        "total": "280",
        "ok": "280",
        "ko": "-"
    },
    "percentiles3": {
        "total": "385",
        "ok": "385",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1350",
        "ok": "1350",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8041,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 338,
    "percentage": 4
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "22.662",
        "ok": "22.662",
        "ko": "-"
    }
},
contents: {
"req_announcements-2b59b": {
        type: "REQUEST",
        name: "Announcements",
path: "Announcements",
pathFormatted: "req_announcements-2b59b",
stats: {
    "name": "Announcements",
    "numberOfRequests": {
        "total": "1677",
        "ok": "1677",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "197",
        "ok": "197",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1378",
        "ok": "1378",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "270",
        "ok": "270",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles1": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "percentiles2": {
        "total": "269",
        "ok": "269",
        "ko": "-"
    },
    "percentiles3": {
        "total": "334",
        "ok": "334",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1243",
        "ok": "1243",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1647,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 30,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.532",
        "ok": "4.532",
        "ko": "-"
    }
}
    },"req_css2-family-pop-2d7d0": {
        type: "REQUEST",
        name: "css2?family=Poppins:wght@400;700&display=swap",
path: "css2?family=Poppins:wght@400;700&display=swap",
pathFormatted: "req_css2-family-pop-2d7d0",
stats: {
    "name": "css2?family=Poppins:wght@400;700&display=swap",
    "numberOfRequests": {
        "total": "1677",
        "ok": "1677",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3261",
        "ok": "3261",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "289",
        "ok": "289",
        "ko": "-"
    },
    "percentiles1": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles2": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "percentiles3": {
        "total": "349",
        "ok": "349",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1247",
        "ok": "1247",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1606,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 4
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.532",
        "ok": "4.532",
        "ko": "-"
    }
}
    },"req_js-id-g-p1nws0b-1f4b6": {
        type: "REQUEST",
        name: "js?id=G-P1NWS0B7MC",
path: "js?id=G-P1NWS0B7MC",
pathFormatted: "req_js-id-g-p1nws0b-1f4b6",
stats: {
    "name": "js?id=G-P1NWS0B7MC",
    "numberOfRequests": {
        "total": "1677",
        "ok": "1677",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "277",
        "ok": "277",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7373",
        "ok": "7373",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "505",
        "ok": "505",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "499",
        "ok": "499",
        "ko": "-"
    },
    "percentiles1": {
        "total": "342",
        "ok": "342",
        "ko": "-"
    },
    "percentiles2": {
        "total": "365",
        "ok": "365",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1349",
        "ok": "1349",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3291",
        "ok": "3291",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1434,
    "percentage": 86
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 243,
    "percentage": 14
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.532",
        "ok": "4.532",
        "ko": "-"
    }
}
    },"req_job-offers-14eea": {
        type: "REQUEST",
        name: "Job offers",
path: "Job offers",
pathFormatted: "req_job-offers-14eea",
stats: {
    "name": "Job offers",
    "numberOfRequests": {
        "total": "1677",
        "ok": "1677",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "81",
        "ok": "81",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "417",
        "ok": "417",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "110",
        "ok": "110",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "105",
        "ok": "105",
        "ko": "-"
    },
    "percentiles2": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "percentiles3": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "percentiles4": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1677,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.532",
        "ok": "4.532",
        "ko": "-"
    }
}
    },"req_see-the-announc-9b430": {
        type: "REQUEST",
        name: "See the announcements",
path: "See the announcements",
pathFormatted: "req_see-the-announc-9b430",
stats: {
    "name": "See the announcements",
    "numberOfRequests": {
        "total": "1677",
        "ok": "1677",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "80",
        "ok": "80",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "407",
        "ok": "407",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "percentiles2": {
        "total": "118",
        "ok": "118",
        "ko": "-"
    },
    "percentiles3": {
        "total": "142",
        "ok": "142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1677,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.532",
        "ok": "4.532",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
