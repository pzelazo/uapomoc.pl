var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "331",
        "ok": "331",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "128",
        "ok": "128",
        "ko": "-"
    },
    "percentiles1": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "percentiles2": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles3": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "percentiles4": {
        "total": "482",
        "ok": "482",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.455",
        "ok": "0.455",
        "ko": "-"
    }
},
contents: {
"req_ogloszenia-5f0b8": {
        type: "REQUEST",
        name: "Ogloszenia",
path: "Ogloszenia",
pathFormatted: "req_ogloszenia-5f0b8",
stats: {
    "name": "Ogloszenia",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "percentiles2": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "percentiles3": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "percentiles4": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    },"req_css2-family-pop-2d7d0": {
        type: "REQUEST",
        name: "css2?family=Poppins:wght@400;700&display=swap",
path: "css2?family=Poppins:wght@400;700&display=swap",
pathFormatted: "req_css2-family-pop-2d7d0",
stats: {
    "name": "css2?family=Poppins:wght@400;700&display=swap",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "percentiles2": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "percentiles3": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "percentiles4": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    },"req_js-id-g-p1nws0b-1f4b6": {
        type: "REQUEST",
        name: "js?id=G-P1NWS0B7MC",
path: "js?id=G-P1NWS0B7MC",
pathFormatted: "req_js-id-g-p1nws0b-1f4b6",
stats: {
    "name": "js?id=G-P1NWS0B7MC",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles2": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles3": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles4": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    },"req_oferty-transpor-dd321": {
        type: "REQUEST",
        name: "Oferty transportu",
path: "Oferty transportu",
pathFormatted: "req_oferty-transpor-dd321",
stats: {
    "name": "Oferty transportu",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "percentiles2": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "percentiles3": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "percentiles4": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    },"req_zobacz-ogloszen-129d9": {
        type: "REQUEST",
        name: "Zobacz ogloszenia",
path: "Zobacz ogloszenia",
pathFormatted: "req_zobacz-ogloszen-129d9",
stats: {
    "name": "Zobacz ogloszenia",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles2": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles3": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles4": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
