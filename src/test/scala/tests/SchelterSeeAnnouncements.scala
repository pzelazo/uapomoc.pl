package tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class SchelterSeeAnnouncements extends Simulation {

  private val httpProtocol = http
    .baseUrl("https://uapomoc.pl")
    .inferHtmlResources(AllowList(), DenyList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""))
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0")
  
  private val headers_0 = Map(
  		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
  		"Accept-Encoding" -> "gzip, deflate, br",
  		"Cache-Control" -> "max-age=0",
  		"Upgrade-Insecure-Requests" -> "1"
  )

  object Announcements {
    val announcements = exec(
      http("Announcements")
        .get("/#rodzaje-ogloszen")
    ).pause(3)
  }

  object OfferingShelter {
    val offeringShelter = exec(
      http("Offering shelter")
        .get("/#schronienie")
    ).pause(3)
  }

  object SeeTheAnnouncements {
    val seeTheAnnouncements = exec(
      http("See the announcements")
        .get("/ogloszenia/oferuje-schronienie")
    ).pause(3)
  }

  val scn = scenario("Zobacz oferty").exec(Announcements.announcements, OfferingShelter.offeringShelter, SeeTheAnnouncements.seeTheAnnouncements)

	setUp(
    scn.inject(
      atOnceUsers(1)))
    .protocols(httpProtocol)
}
