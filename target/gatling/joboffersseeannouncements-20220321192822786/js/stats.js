var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "19506",
        "ok": "9430",
        "ko": "10076"
    },
    "minResponseTime": {
        "total": "63",
        "ok": "63",
        "ko": "10000"
    },
    "maxResponseTime": {
        "total": "20117",
        "ok": "17353",
        "ko": "20117"
    },
    "meanResponseTime": {
        "total": "8699",
        "ok": "2132",
        "ko": "14846"
    },
    "standardDeviation": {
        "total": "7742",
        "ok": "3717",
        "ko": "4998"
    },
    "percentiles1": {
        "total": "10001",
        "ok": "230",
        "ko": "10010"
    },
    "percentiles2": {
        "total": "18069",
        "ok": "3205",
        "ko": "20001"
    },
    "percentiles3": {
        "total": "20003",
        "ok": "11223",
        "ko": "20004"
    },
    "percentiles4": {
        "total": "20007",
        "ok": "17237",
        "ko": "20011"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5801,
    "percentage": 30
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3596,
    "percentage": 18
},
    "group4": {
    "name": "failed",
    "count": 10076,
    "percentage": 52
},
    "meanNumberOfRequestsPerSecond": {
        "total": "46.333",
        "ok": "22.399",
        "ko": "23.933"
    }
},
contents: {
"req_announcements-2b59b": {
        type: "REQUEST",
        name: "Announcements",
path: "Announcements",
pathFormatted: "req_announcements-2b59b",
stats: {
    "name": "Announcements",
    "numberOfRequests": {
        "total": "3516",
        "ok": "1438",
        "ko": "2078"
    },
    "minResponseTime": {
        "total": "199",
        "ok": "199",
        "ko": "20000"
    },
    "maxResponseTime": {
        "total": "20117",
        "ok": "17344",
        "ko": "20117"
    },
    "meanResponseTime": {
        "total": "13632",
        "ok": "4427",
        "ko": "20003"
    },
    "standardDeviation": {
        "total": "8315",
        "ok": "5068",
        "ko": "5"
    },
    "percentiles1": {
        "total": "20001",
        "ok": "1274",
        "ko": "20002"
    },
    "percentiles2": {
        "total": "20002",
        "ok": "7286",
        "ko": "20003"
    },
    "percentiles3": {
        "total": "20005",
        "ok": "13277",
        "ko": "20007"
    },
    "percentiles4": {
        "total": "20013",
        "ok": "17269",
        "ko": "20016"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 515,
    "percentage": 15
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 923,
    "percentage": 26
},
    "group4": {
    "name": "failed",
    "count": 2078,
    "percentage": 59
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.352",
        "ok": "3.416",
        "ko": "4.936"
    }
}
    },"req_css2-family-pop-2d7d0": {
        type: "REQUEST",
        name: "css2?family=Poppins:wght@400;700&display=swap",
path: "css2?family=Poppins:wght@400;700&display=swap",
pathFormatted: "req_css2-family-pop-2d7d0",
stats: {
    "name": "css2?family=Poppins:wght@400;700&display=swap",
    "numberOfRequests": {
        "total": "4479",
        "ok": "2090",
        "ko": "2389"
    },
    "minResponseTime": {
        "total": "63",
        "ok": "63",
        "ko": "10000"
    },
    "maxResponseTime": {
        "total": "10156",
        "ok": "7370",
        "ko": "10156"
    },
    "meanResponseTime": {
        "total": "5960",
        "ok": "1338",
        "ko": "10003"
    },
    "standardDeviation": {
        "total": "4565",
        "ok": "2148",
        "ko": "12"
    },
    "percentiles1": {
        "total": "10000",
        "ok": "233",
        "ko": "10001"
    },
    "percentiles2": {
        "total": "10001",
        "ok": "1236",
        "ko": "10002"
    },
    "percentiles3": {
        "total": "10003",
        "ok": "7228",
        "ko": "10006"
    },
    "percentiles4": {
        "total": "10014",
        "ok": "7251",
        "ko": "10054"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1351,
    "percentage": 30
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 706,
    "percentage": 16
},
    "group4": {
    "name": "failed",
    "count": 2389,
    "percentage": 53
},
    "meanNumberOfRequestsPerSecond": {
        "total": "10.639",
        "ok": "4.964",
        "ko": "5.675"
    }
}
    },"req_js-id-g-p1nws0b-1f4b6": {
        type: "REQUEST",
        name: "js?id=G-P1NWS0B7MC",
path: "js?id=G-P1NWS0B7MC",
pathFormatted: "req_js-id-g-p1nws0b-1f4b6",
stats: {
    "name": "js?id=G-P1NWS0B7MC",
    "numberOfRequests": {
        "total": "4479",
        "ok": "1672",
        "ko": "2807"
    },
    "minResponseTime": {
        "total": "92",
        "ok": "92",
        "ko": "10000"
    },
    "maxResponseTime": {
        "total": "10130",
        "ok": "7478",
        "ko": "10130"
    },
    "meanResponseTime": {
        "total": "7187",
        "ok": "2461",
        "ko": "10002"
    },
    "standardDeviation": {
        "total": "3927",
        "ok": "2382",
        "ko": "8"
    },
    "percentiles1": {
        "total": "10001",
        "ok": "1348",
        "ko": "10001"
    },
    "percentiles2": {
        "total": "10001",
        "ok": "3342",
        "ko": "10002"
    },
    "percentiles3": {
        "total": "10004",
        "ok": "7348",
        "ko": "10006"
    },
    "percentiles4": {
        "total": "10016",
        "ok": "7371",
        "ko": "10034"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 449,
    "percentage": 10
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1223,
    "percentage": 27
},
    "group4": {
    "name": "failed",
    "count": 2807,
    "percentage": 63
},
    "meanNumberOfRequestsPerSecond": {
        "total": "10.639",
        "ok": "3.971",
        "ko": "6.667"
    }
}
    },"req_job-offers-14eea": {
        type: "REQUEST",
        name: "Job offers",
path: "Job offers",
pathFormatted: "req_job-offers-14eea",
stats: {
    "name": "Job offers",
    "numberOfRequests": {
        "total": "3516",
        "ok": "1915",
        "ko": "1601"
    },
    "minResponseTime": {
        "total": "81",
        "ok": "81",
        "ko": "20000"
    },
    "maxResponseTime": {
        "total": "20078",
        "ok": "17353",
        "ko": "20078"
    },
    "meanResponseTime": {
        "total": "10164",
        "ok": "1938",
        "ko": "20002"
    },
    "standardDeviation": {
        "total": "9510",
        "ok": "4177",
        "ko": "5"
    },
    "percentiles1": {
        "total": "10283",
        "ok": "116",
        "ko": "20002"
    },
    "percentiles2": {
        "total": "20001",
        "ok": "221",
        "ko": "20003"
    },
    "percentiles3": {
        "total": "20004",
        "ok": "13215",
        "ko": "20006"
    },
    "percentiles4": {
        "total": "20009",
        "ok": "17252",
        "ko": "20016"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1497,
    "percentage": 43
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 418,
    "percentage": 12
},
    "group4": {
    "name": "failed",
    "count": 1601,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.352",
        "ok": "4.549",
        "ko": "3.803"
    }
}
    },"req_see-the-announc-9b430": {
        type: "REQUEST",
        name: "See the announcements",
path: "See the announcements",
pathFormatted: "req_see-the-announc-9b430",
stats: {
    "name": "See the announcements",
    "numberOfRequests": {
        "total": "3516",
        "ok": "2315",
        "ko": "1201"
    },
    "minResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "20000"
    },
    "maxResponseTime": {
        "total": "20061",
        "ok": "17338",
        "ko": "20061"
    },
    "meanResponseTime": {
        "total": "7719",
        "ok": "1347",
        "ko": "20002"
    },
    "standardDeviation": {
        "total": "9320",
        "ok": "3613",
        "ko": "4"
    },
    "percentiles1": {
        "total": "136",
        "ok": "114",
        "ko": "20001"
    },
    "percentiles2": {
        "total": "20001",
        "ok": "134",
        "ko": "20002"
    },
    "percentiles3": {
        "total": "20003",
        "ok": "11228",
        "ko": "20006"
    },
    "percentiles4": {
        "total": "20007",
        "ok": "17254",
        "ko": "20014"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1989,
    "percentage": 57
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 326,
    "percentage": 9
},
    "group4": {
    "name": "failed",
    "count": 1201,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.352",
        "ok": "5.499",
        "ko": "2.853"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
