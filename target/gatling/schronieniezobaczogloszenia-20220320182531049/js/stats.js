var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "964",
        "ok": "964",
        "ko": "-"
    },
    "percentiles1": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "percentiles2": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2787",
        "ok": "2787",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 80
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 20
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.357",
        "ok": "0.357",
        "ko": "-"
    }
},
contents: {
"req_ogloszenia-5f0b8": {
        type: "REQUEST",
        name: "Ogloszenia",
path: "Ogloszenia",
pathFormatted: "req_ogloszenia-5f0b8",
stats: {
    "name": "Ogloszenia",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.071",
        "ok": "0.071",
        "ko": "-"
    }
}
    },"req_css2-family-pop-2d7d0": {
        type: "REQUEST",
        name: "css2?family=Poppins:wght@400;700&display=swap",
path: "css2?family=Poppins:wght@400;700&display=swap",
pathFormatted: "req_css2-family-pop-2d7d0",
stats: {
    "name": "css2?family=Poppins:wght@400;700&display=swap",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "percentiles2": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "percentiles3": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "percentiles4": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.071",
        "ok": "0.071",
        "ko": "-"
    }
}
    },"req_js-id-g-p1nws0b-1f4b6": {
        type: "REQUEST",
        name: "js?id=G-P1NWS0B7MC",
path: "js?id=G-P1NWS0B7MC",
pathFormatted: "req_js-id-g-p1nws0b-1f4b6",
stats: {
    "name": "js?id=G-P1NWS0B7MC",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "percentiles2": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "percentiles3": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "percentiles4": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.071",
        "ok": "0.071",
        "ko": "-"
    }
}
    },"req_oferty-pracy-cb82d": {
        type: "REQUEST",
        name: "Oferty pracy",
path: "Oferty pracy",
pathFormatted: "req_oferty-pracy-cb82d",
stats: {
    "name": "Oferty pracy",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "percentiles2": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "percentiles3": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "percentiles4": {
        "total": "527",
        "ok": "527",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.071",
        "ok": "0.071",
        "ko": "-"
    }
}
    },"req_zobacz-ogloszen-129d9": {
        type: "REQUEST",
        name: "Zobacz ogloszenia",
path: "Zobacz ogloszenia",
pathFormatted: "req_zobacz-ogloszen-129d9",
stats: {
    "name": "Zobacz ogloszenia",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "percentiles2": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "percentiles3": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "percentiles4": {
        "total": "630",
        "ok": "630",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.071",
        "ok": "0.071",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
